﻿module SegmentationModule

open SegmentModule

// Maps segments to their immediate parent segment that they are contained within (if any) 
type Segmentation = Map<Segment, Segment>

// Find the largest/top level segment that the given segment is a part of (based on the current segmentation)
let rec findRoot (segmentation: Segmentation) segment : Segment =
    if (Map.containsKey segment segmentation) then
        let parent = segmentation.[segment]
        findRoot segmentation parent
    else
        segment


// Initially, every pixel/coordinate in the image is a separate Segment
// Note: this is a higher order function which given an image, 
// returns a function which maps each coordinate to its corresponding (initial) Segment (of kind Pixel)
let createPixelMap (image:TiffModule.Image) : (Coordinate -> Segment) =
    let mapPixel (x,y) = Pixel((x,y),TiffModule.getColourBands image (x,y))
    mapPixel

// Find the neighbouring segments of the given segment (assuming we are only segmenting the top corner of the image of size 2^N x 2^N)
// Note: this is a higher order function which given a pixelMap function and a size N, 
// returns a function which given a current segmentation, returns the set of Segments which are neighbours of a given segment
let createNeighboursFunction (pixelMap:Coordinate->Segment) (N:int) : (Segmentation -> Segment -> Set<Segment>) =
    let width = 1<<<N
    let height = 1<<<N
    let rec neighborPixels segment =
        match segment with
            |Pixel((x,y),colour) ->
                seq 
                    {
                    if x+1 < width then yield pixelMap(x+1,y)
                    if x-1 >= 0 then yield pixelMap(x-1,y)
                    if y+1 < height then yield pixelMap(x,y+1)
                    if y-1 >= 0 then yield pixelMap(x,y-1)
                    }
            |Parent(left,right) -> 
                seq
                    {
                    yield! neighborPixels left
                    yield! neighborPixels right
                    }
    
    let neighborsSequence segmentation segment = (neighborPixels segment)|> Seq.map(fun segment -> findRoot segmentation segment)|>Set.ofSeq 
    let neighbors segmentation segment = (neighborsSequence segmentation segment) |> Set.remove(findRoot segmentation segment)
    neighbors


// The following are also higher order functions, which given some inputs, return a function which ...


 // Find the neighbour(s) of the given segment that has the (equal) best merge cost
 // (exclude neighbours if their merge cost is greater than the threshold)
let createBestNeighbourFunction (neighbours:Segmentation->Segment->Set<Segment>) (threshold:float) : (Segmentation->Segment->Set<Segment>) =
    
    let noNeighbors = Set.empty
    let bestNeighbors segmentation segment1= 
        let neighbourSet = neighbours segmentation segment1
        if not(neighbourSet |> Set.isEmpty) then
            let mergedCost = neighbourSet |> Set.filter(fun seg -> ((mergeCost segment1 seg) <= threshold))
            if not(mergedCost |> Set.isEmpty) then
                let minCost = mergedCost |> Set.map(mergeCost segment1) |> Set.minElement
                let filteredCost = mergedCost |> Set.filter(fun seg -> ((mergeCost segment1 seg) <= minCost))
                if not(filteredCost |> Set.isEmpty) then filteredCost else noNeighbors
            else noNeighbors
        else noNeighbors
    
    bestNeighbors

// Try to find a neighbouring segmentB such that:
//     1) segmentB is one of the best neighbours of segment A, and 
//     2) segmentA is one of the best neighbours of segment B
// if such a mutally optimal neighbour exists then merge them,
// otherwise, choose one of segmentA's best neighbours (if any) and try to grow it instead (gradient descent)
let createTryGrowOneSegmentFunction (bestNeighbours:Segmentation->Segment->Set<Segment>) (pixelMap:Coordinate->Segment) : (Segmentation->Coordinate->Segmentation) =
    
    let rec TryGrowOneSegment segmentation segmentA =
        let bestNeighbourSet = bestNeighbours segmentation segmentA
        if not(bestNeighbourSet |> Set.isEmpty) then
            let bestNeighbourSetB = bestNeighbours segmentation segmentB
            if not(bestNeighbourSetB |> Set.isEmpty) then
                if (bestNeighbourSetB |> Set.contains(segmentA)) then
                    let mergeSegment segmentA segmentB = Parent(segmentA, segmentB)
                    let newSegment = mergeSegment segmentA segmentB
                    newSegment
                else 
        
    //raise (System.NotImplementedException())
    // Fixme: add implementation here


// Try to grow the segments corresponding to every pixel on the image in turn 
// (considering pixel coordinates in special dither order)
let createTryGrowAllCoordinatesFunction (tryGrowPixel:Segmentation->Coordinate->Segmentation) (N:int) : (Segmentation->Segmentation) =
    raise (System.NotImplementedException())
    // Fixme: add implementation here


// Keep growing segments as above until no further merging is possible
let createGrowUntilNoChangeFunction (tryGrowAllCoordinates:Segmentation->Segmentation) : (Segmentation->Segmentation) =
    raise (System.NotImplementedException())
    // Fixme: add implementation here


// Segment the given image based on the given merge cost threshold, but only for the top left corner of the image of size (2^N x 2^N)
let segment (image:TiffModule.Image) (N: int) (threshold:float)  : (Coordinate -> Segment) =
    raise (System.NotImplementedException())
    // Fixme: use the functions above to help implement this function