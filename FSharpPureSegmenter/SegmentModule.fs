﻿module SegmentModule

type Coordinate = (int * int) // x, y coordinate of a pixel
type Colour = byte list       // one entry for each colour band, typically: [red, green and blue]

type Segment = 
    | Pixel of Coordinate * Colour
    | Parent of Segment * Segment 

// return a list of the standard deviations of the pixel colours in the given segment
// the list contains one entry for each colour band, typically: [red, green and blue]
let stddev (segment: Segment) : float list =
    
    let rec colourlist segment =
        seq {
            match segment with
                | Parent(left, right) ->
                    yield! colourlist left
                    yield! colourlist right
                | Pixel(_, colourbytes) -> yield (colourbytes |> List.map ( fun x -> float x ) )
         }
      
    let transpose rows =
        let n = Seq.length(Seq.head rows)
        Seq.init n (fun i -> Seq.toList(Seq.map (Seq.item i) rows))

    let averageList list = (List.fold ( fun acc elem -> acc + elem ) 0.0 list / float list.Length)

    let stdDevColours list=
        let avg = averageList list
        sqrt (List.fold (fun acc elem -> acc + (elem - avg) ** 2.0) 0.0 list / float list.Length)

    let getStdDevList colsequence = Seq.toList (colsequence |> Seq.map (fun list -> stdDevColours list))
    
    let colours = transpose ( colourlist segment )
    
    getStdDevList colours

// determine the cost of merging the given segments: 
// equal to the standard deviation of the combined the segments minus the sum of the standard deviations of the individual segments, 
// weighted by their respective sizes and summed over all colour bands
let mergeCost segment1 segment2 : float = 

    let mergedsegment = Parent(segment1, segment2)

    let stdDevOne = List.sum (stddev segment1)
    let stdDevTwo = List.sum (stddev segment2)
    let mergedStdDev = List.sum (stddev mergedsegment)

    let countPixels segment =
        let rec Loop acc = function
                | Pixel(_) -> (1 + acc)
                | Parent(left, right) ->
                    acc + (Loop acc left) + (Loop acc right) 
        Loop 0 segment

    mergedStdDev * float (countPixels mergedsegment) - (stdDevOne * float (countPixels segment1) + stdDevTwo * float (countPixels segment2)) 

 